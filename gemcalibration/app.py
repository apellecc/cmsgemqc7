""" Master script """

import os
import time
import pathlib
import argparse

import inquirer
import yaml

from gemcalibration.src import gemdaq
from gemcalibration.src import tree_parser
from gemcalibration.src import scans

username = os.getlogin()
SCAN_DATA_PATH = pathlib.Path(f"/tmp/{username}/gemdata")


def run():
    parser = argparse.ArgumentParser()
    parser.add_argument("config", type=pathlib.Path, help="YAML config file")
    args = parser.parse_args()

    with open(args.config, "r") as stream:
        setup = yaml.safe_load(stream)

    layout_tree = tree_parser.parse_layout(setup["layout_tree"])

    gemos = gemdaq.GemosJson(setup, layout_tree, scan_path=SCAN_DATA_PATH)
    fsm_state = gemos.get_state()
    print("The FSM state is", fsm_state)

    """ If not configured, initialized and configure FSM """
    if gemos.get_state() == "Initial":
        print("Initializing...")
        gemos.transition("Initialize")
        while gemos.get_state() == "Initializing":
            time.sleep(1)
        print(gemos.get_state())
    if gemos.get_state() == "Halted":
        print("Configuring...")
        gemos.transition("Configure")
        while gemos.get_state() == "Configuring":
            time.sleep(1)
        print(gemos.get_state())

    """ If configured, prompt for scans to be performed """
    if gemos.get_state() == "Configured":
        choices = scans.CALIBRATION_PARAMETERS.keys()
        questions = [
            inquirer.Checkbox(
                "scans",
                message="Select scans to be performed",
                choices=choices,
                default=choices,
            )
        ]
        chosen_scans = inquirer.prompt(questions)

        for scan in chosen_scans["scans"]:

            # Prepare DAQ and start scan:
            print(f"Starting {scan}...")
            gemos.scan_manager.start_scan(scan)
            print(f"{scan} ongoing...")

            # Wait for scan to be over:
            calibration_state = True
            while calibration_state:
                calibration_state = gemos.scan_manager.get_calibration_state()
                time.sleep(1)
            print(f"{scan} completed.")

            # Analyse scan results:
            print(f"Analyzing {scan}...")
            gemos.scan_manager.analyze_scan(scan)
            print("")

            print(f"Applying {scan} results...")
            gemos.scan_manager.copy_config(scan)
            print("") 
