""" Interface to the GEM DAQ software """

import os, pathlib, shutil

import warnings

warnings.simplefilter(action="ignore", category=FutureWarning)

from gemos.analysis import dac_scan_analysis
from gemos.analysis import gbt_phase_scan
from gemos.analysis import threshold_scan_analysis
from gemos.analysis import vfat_parameters

CALIBRATION_PARAMETERS = {
    # "Physics",
    "GBT Rx Phase Scan": {
        "cal_type_select": "GBT Rx Phase Scan",
        "doVFATPhaseScan": "0",
        "nTransactions": "50",
        "nTriggers": "1000000",
    },
    # "Latency Scan",
    # "S-curve Scan",
    # "ARM DAC Scan",
    # "Derive DAC Trim Registers",
    "DAC Scan on VFAT": {
        "cal_type_select": "DAC Scan on VFAT",
        "adcExtType": "0",
        "CFG_CAL_DAC_Min": "0",
        "CFG_CAL_DAC_Max": "255",
        "CFG_BIAS_PRE_I_BIT": "on",
        "CFG_BIAS_PRE_I_BIT_Min": "0",
        "CFG_BIAS_PRE_I_BIT_Max": "255",
        "CFG_BIAS_PRE_I_BLCC": "on",
        "CFG_BIAS_PRE_I_BLCC_Min": "0",
        "CFG_BIAS_PRE_I_BLCC_Max": "63",
        "CFG_BIAS_PRE_I_BSF": "on",
        "CFG_BIAS_PRE_I_BSF_Min": "0",
        "CFG_BIAS_PRE_I_BSF_Max": "63",
        "CFG_BIAS_SH_I_BFCAS": "on",
        "CFG_BIAS_SH_I_BFCAS_Min": "0",
        "CFG_BIAS_SH_I_BFCAS_Max": "255",
        "CFG_BIAS_SH_I_BDIFF": "on",
        "CFG_BIAS_SH_I_BDIFF_Min": "0",
        "CFG_BIAS_SH_I_BDIFF_Max": "255",
        "CFG_BIAS_SD_I_BDIFF": "on",
        "CFG_BIAS_SD_I_BDIFF_Min": "0",
        "CFG_BIAS_SD_I_BDIFF_Max": "255",
        "CFG_BIAS_SD_I_BFCAS": "on",
        "CFG_BIAS_SD_I_BFCAS_Min": "0",
        "CFG_BIAS_SD_I_BFCAS_Max": "255",
        "CFG_BIAS_SD_I_BSF": "on",
        "CFG_BIAS_SD_I_BSF_Min": "0",
        "CFG_BIAS_SD_I_BSF_Max": "63",
        "CFG_BIAS_CFD_DAC_1": "on",
        "CFG_BIAS_CFD_DAC_1_Min": "0",
        "CFG_BIAS_CFD_DAC_1_Max": "63",
        "CFG_BIAS_CFD_DAC_2": "on",
        "CFG_BIAS_CFD_DAC_2_Min": "0",
        "CFG_BIAS_CFD_DAC_2_Max": "63",
        "CFG_HYST": "on",
        "CFG_HYST_Min": "0",
        "CFG_HYST_Max": "63",
        "CFG_THR_ARM_DAC_Min": "0",
        "CFG_THR_ARM_DAC_Max": "255",
        "CFG_THR_ZCC_DAC_Min": "0",
        "CFG_THR_ZCC_DAC_Max": "255",
        "CFG_BIAS_PRE_VREF": "on",
        "CFG_BIAS_PRE_VREF_Min": "0",
        "CFG_BIAS_PRE_VREF_Max": "255",
        "CFG_VREF_ADC_Min": "0",
        "CFG_VREF_ADC_Max": "3",
    },
    "S-bit ARM DAC Scan": {
        "cal_type_select": "S-bit ARM DAC Scan",
        "iterNum": "6",
        "scanMax": "255",
        "scanMin": "0",
        "stepSize": "1",
        "timeInterval": "1",
        "toggleRunMode": "1",
        "vfatChMax": "127",
        "vfatChMin": "0",
    },
    # "Calibrate CFG_THR_ARM_DAC"
}


class ScanManager:
    def __init__(self, gemos):
        self.gemos = gemos
        self.paths = {
            "mapping": pathlib.Path(self.gemos.layout_tree.vfat_mapping_path),
            "dacScan": None,
            "sBitRateScan": None,
        }
        self.default_paths = self.gemos.default_paths

    @property
    def scan_path(self):
        return self.gemos.scan_path

    def get_calibration_state(self):
        state_json = self.gemos.get_json(
            self.gemos.app_id["gemmanager"], "calibrationStateUpdate"
        )
        return state_json["calibrationState"]

    def set_scan_type(self, scan_type):
        return self.gemos.request(
            self.gemos.app_id["gemcalibration"],
            f"setCalInterface?cal_type_select={scan_type}",
        )

    def apply_scan_params(self, params):
        body_json = self.gemos.get_json(
            self.gemos.app_id["gemcalibration"], "applyAction", data=params
        )
        if body_json["status"] != 0:
            raise Exception(
                "Error applying calibration parameters: status {}".format(
                    body_json["status"]
                )
            )
        return body_json["alert"]

    def run_scan(self):
        self.gemos.request(self.gemos.app_id["gemcalibration"], "runScan")

    def start_scan(self, scan_type):
        self.set_scan_type(scan_type)
        self.apply_scan_params(CALIBRATION_PARAMETERS[scan_type])
        if scan_type in ["DAC Scan on VFAT", "S-bit ARM DAC Scan"]:
            # start and stop a run to configure the VFATs is not necessary any more:
            # self.gemos.start_and_stop()
            oh_mask = self.gemos.oh_mask
            # print(self.gemos.oh_mask, self.gemos.layout_tree.oh_mask)
            if self.gemos.oh_mask != self.gemos.layout_tree.oh_mask:
                raise Exception("There are masked OptoHybrids: {}".format(oh_mask))
        self.run_scan()

    # TEMPORARY DEBUGGING FUNCTION, TO BE REMOVED:
    def get_output_file(self, scan_type):
        if scan_type == "GBT Rx Phase Scan":
            return self.scan_path / "gbtPhaseScan/fed1479-slot07.cfg"
        elif scan_type == "DAC Scan on VFAT":
            return self.scan_path / "dacScan/fed1479-slot07.txt"
        elif scan_type == "S-bit ARM DAC Scan":
            return self.scan_path / "sBitRateScan/fed1479-slot07.txt"

    def get_output_files(self, scan_type):
        dirname = {
            "GBT Rx Phase Scan": "gbtPhaseScan",
            "DAC Scan on VFAT": "dacScan",
            "S-bit ARM DAC Scan": "sBitRateScan",
        }
        extensions = {
            "GBT Rx Phase Scan": "cfg",
            "DAC Scan on VFAT": "txt",
            "S-bit ARM DAC Scan": "txt",
        }
        return [
            self.scan_path
            / dirname[scan_type]
            / "fed{:02d}-slot{:02d}.{}".format(
                fed.number, slot.number, extensions[scan_type]
            )
            for fed in self.gemos.layout_tree.feds
            for slot in fed.amcs
        ]

    def get_configuration_path(self, component):
        if component == "gbt":
            return self.scan_path / "config/gbt"
        elif component == "vfat":
            return self.scan_path / "config/vfat"
        else:
            raise ValueError("Unknown component type: " + component)

    def analyze_scan(self, scan_type):
        if scan_type == "GBT Rx Phase Scan":
            print("Creating GBT config file...")
            self.create_config("gbt")
        elif scan_type == "DAC Scan on VFAT":
            self.paths["dacScan"] = [dac_scan_analysis.create_configuration(
                input_filenames=self.get_output_files(scan_type),
                output_dir=self.scan_path / "results/dacScan",
                mapping_filename=self.paths["mapping"],
                calibration_filename=self.gemos.vfat_db_file,
            )]
            print("Files saved in {}".format(self.paths["dacScan"][0]))
            print("Creating VFAT config file...")
            self.create_config("vfat")
        elif scan_type == "S-bit ARM DAC Scan":
            self.paths["sBitRateScan"] = [threshold_scan_analysis.create_configuration(
                input_filenames=self.get_output_files(scan_type),
                output_directory=self.scan_path / "results/sBitRateScan",
                target_rate=100,
            )]
            print("Files saved in", self.paths["sBitRateScan"])
            print("Creating VFAT config file...")
            self.create_config("vfat")
        else:
            print("Nothing to do or analysis not implemented yet.")

    def create_config(self, component):
        if component == "gbt":
            gbt_config_files = gbt_phase_scan.create_configuration(
                input_filenames=self.get_output_files("GBT Rx Phase Scan"),
                output_directory=self.get_configuration_path("gbt"),
            )
            print("Files saved to:")
            print(gbt_config_files)
            self.paths["gbtConfigFiles"] = gbt_config_files
        elif component == "vfat":
            print("Using default configuration from {}".format(self.default_paths["vfat"]))
            vfat_config_files = vfat_parameters.create_configuration(
                input_directory=self.get_configuration_path("vfat"),
                output_directory=self.get_configuration_path("vfat"),
                default_filename=self.default_paths["vfat"],
                mapping_filename=self.paths["mapping"],
                calibration_filename=self.gemos.vfat_db_file,
                dac_filenames=self.paths["dacScan"],
                threshold_filenames=self.paths["sBitRateScan"],
                latency_filenames=None,
            )
            print("Files saved to:")
            print(vfat_config_files)
            self.paths["vfatConfigFiles"] = vfat_config_files

    def copy_config(self, scan):
        """ For file destination see https://gitlab.cern.ch/cmsgemonline/gem-daq/cmsgemos/-/blob/main/doc/configuration-files.md """
        try:
            for gbt_config_file in self.paths["gbtConfigFiles"]:
                gbt_config_directory = self.gemos.daq_path / "etc/cmsgemos/gbt"
                os.makedirs(gbt_config_directory, exist_ok=True)
                print("Created file", shutil.copy(gbt_config_file, gbt_config_directory))
            for vfat_config_file in self.paths["vfatConfigFiles"]:
                vfat_config_directory = self.gemos.daq_path / "etc/cmsgemos/vfat"
                os.makedirs(vfat_config_directory, exist_ok=True)
                print("Created file", shutil.copy(vfat_config_file, vfat_config_directory))
        except KeyError as e:
            print("Could not copy configuration files:", e)

