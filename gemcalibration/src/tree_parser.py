import os, re
import yaml
import pandas as pd


def parse_layout(tree_directory):
    root_file = os.path.join(tree_directory, "99-root.yml")
    node_directory = os.path.join(tree_directory, "00-links.d")
    node_filenames = os.listdir(node_directory)
    node_files = [os.path.join(node_directory, f) for f in node_filenames]
    yaml_text = ""
    for node_file in node_files:
        with open(node_file, "r") as stream:
            yaml_text += stream.read()
    with open(root_file, "r") as stream:
        yaml_text += stream.read()
    # strip leading zeros in IDs to avoid conversion to octal:
    yaml_text = re.sub(r"\b0(\d+)\b", r"\1", yaml_text)
    layout_tree = yaml.load(yaml_text, Loader=yaml.FullLoader)
    return LayoutTree(layout_tree["root"])


class LayoutTree:
    def __init__(self, tree):
        self.tree = tree

    @property
    def feds(self):
        return self.tree.children

    @property
    def vfat_mapping(self):
        print("Creating VFAT mapping file...")
        mapping_dict = {
            "fed": list(),
            "slot": list(),
            "oh": list(),
            "vfat": list(),
            "chip-id": list(),
        }
        for fed in self.feds:
            for amc in fed.amcs:
                for link in amc.links:
                    for vfat in link.vfats:
                        mapping_dict["fed"].append(fed.number)
                        mapping_dict["slot"].append(amc.number)
                        mapping_dict["oh"].append(link.number)
                        mapping_dict["vfat"].append(vfat.number)
                        mapping_dict["chip-id"].append(vfat.uid)
        mapping_df = pd.DataFrame.from_dict(mapping_dict)
        print(mapping_df, "\n")
        return mapping_df

    @property
    def vfat_mapping_path(self):
        # Create file in /tmp with VFAT mapping, to be passed to analysis code
        try:
            return self._vfat_mapping_path
        except AttributeError:
            self.vfat_mapping.to_csv("/tmp/vfat_mapping.csv", sep=";", index=False)
            self._vfat_mapping_path = "/tmp/vfat_mapping.csv"
            return self._vfat_mapping_path

    @property
    def oh_mask(self):
        return 0


class Root(yaml.YAMLObject):
    yaml_tag = "!root"

    def __init__(self, display_name, uid, children):
        self.display_name = display_name
        self.uid = uid
        self.children = children


class Fed(yaml.YAMLObject):
    yaml_tag = "!fed"

    def __init__(self, number, name, uid, children):
        self.number = number
        self.name = name
        self.uid = uid
        self.children = children

    @property
    def amcs(self):
        return [child for child in self.children if isinstance(child, BackendBoard)]


class Amc13(yaml.YAMLObject):
    yaml_tag = "!amc13"

    def __init__(self, t1uri, t2uri, uid):
        self.t1uri = t1uri
        self.t2uri = t2uri
        self.uid = uid


class BackendBoard(yaml.YAMLObject):
    yaml_tag = "!backend_board"

    def __init__(self, number, hostname, uid, children):
        self.number = number
        self.hostname = hostname
        self.uid = uid
        self.children = children

    @property
    def links(self):
        return self.children


class Link(yaml.YAMLObject):
    yaml_tag = "!link"

    def __init__(self, number, name, wire_name, gem_type, uid, children):
        self.number = number
        self.name = name
        self.wire_name = wire_name
        self.gem_type = gem_type
        self.uid = uid
        self.children = children

    @property
    def vfats(self):
        return [child for child in self.children if isinstance(child, Vfat)]


class OhFpga(yaml.YAMLObject):
    yaml_tag = "!optohybrid_fpga"

    def __init__(self, val):
        self.val = val


class Gbt(yaml.YAMLObject):
    yaml_tag = "!gbt"

    def __init__(self, val):
        self.val = val


class Vfat(yaml.YAMLObject):
    yaml_tag = "!vfat"

    def __init__(self, val):
        # convert from
        self.val = val
