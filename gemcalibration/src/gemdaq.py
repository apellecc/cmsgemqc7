""" Interface to the GEM DAQ software """

import os, pathlib
import time
import requests

from gemcalibration.src import scans


class GemosJson:
    def __init__(self, setup, layout_tree, scan_path):
        self.baseurl = "http://{}:{}".format(setup["host"], setup["port"])
        self.app_id = setup["app_id"]
        self.vfat_db_file = setup["vfat_db"]
        self.vfat_mapping_file = setup["vfat_mapping"]
        self.default_paths = setup["defaults"]
        self.daq_path = pathlib.Path(setup["cmsgemos_install"])
        self.layout_tree = layout_tree
        self.scan_path = scan_path
        self.scan_manager = scans.ScanManager(self)

    def request(self, app_id, command, data=None):
        if data is None:
            state_body = requests.get(
                f"{self.baseurl}/urn:xdaq-application:lid={app_id}/{command}"
            )
        else:
            state_body = requests.post(
                f"{self.baseurl}/urn:xdaq-application:lid={app_id}/{command}", data=data
            )
        if state_body.status_code != requests.codes.ok:
            state_body.raise_for_status()
        return state_body

    def get_json(self, app_id, command, data=None):
        state_body = self.request(app_id, command, data)
        return state_body.json()

    def get_state(self):
        state = self.get_json(self.app_id["gemsupervisor"], "stateUpdate")
        return state["value"]

    def get_update(self):
        state = self.get_json(self.app_id["gemsupervisor"], "jsonUpdate")
        return state["states"]

    def transition(self, transition_name):
        self.request(self.app_id["gemsupervisor"], transition_name)

    def start_run(self):
        self.transition("Start")

    def stop_run(self):
        self.transition("Stop")

    @property
    def oh_mask(self):
        return 0

    def start_and_stop(self):
        # start and stop a run to configure the VFATs
        # needed before starting some scans (scurves, DAC scans, s-bit rate scans)
        self.start_run()
        while self.get_state() == "Starting":
            time.sleep(1)
        if self.get_state() != "Running":
            raise Exception("Could not start run. State is " + self.get_state())
        time.sleep(5)
        self.stop_run()
        while self.get_state() == "Stopping":
            time.sleep(1)
        if self.get_state() != "Configured":
            raise Exception("Could not stop run. State is: " + self.get_state())
        time.sleep(1)
