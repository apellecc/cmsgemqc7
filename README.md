Master script for the CMS GEM quality control of the on-chamber electronics.

## Installation

```bash
git clone https://gitlab.cern.ch/apellecc/cmsgemqc7.git
cd cmsgemqc7
source env.sh
poetry install
```

## Setup and usage

```bash
source env.sh
poetry shell
gemcalibration examples/config.yml
```