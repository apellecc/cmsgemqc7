#!/bin/sh

source scl_source enable rh-git218 # Newer Git release (optional)
source scl_source enable rh-python38 # Python 3.8 release (optional)
export PATH=/opt/poetry/bin:$PATH # Poetry